from setuptools import find_packages, setup
import versioneer

setup(
    name='alignment',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description='alignment pipeline built on top of biowrappers',
    author='',
    author_email='',
    url='',
    entry_points={'console_scripts': ['alignment = alignment.run:main']},
    packages=find_packages(),
)


