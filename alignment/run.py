import pypeliner
import yaml
import argparse
from workflows import alignment


def load_yaml(path):
    try:
        with open(path) as infile:
            data = yaml.load(infile)

    except IOError:
        raise Exception(
            'Unable to open file: {0}'.format(path))
    return data


def read_bams(bams):

    data = load_yaml(bams)

    for sample in data.keys():
        assert "fastq1" in data[sample],\
            "couldnt extract fastq1 file path "\
            "from yaml input for sample: {}".format(sample)
        assert "fastq2" in data[sample],\
            "couldnt extract fastq2 file path "\
            "from yaml input for sample: {}".format(sample)
        assert "output" in data[sample],\
            "couldnt extract output bam file path "\
            "from yaml input for sample: {}".format(sample)

    fastq1_filenames = {sample: data[sample]["fastq1"]
                           for sample in data.keys()}
    fastq2_filenames = {sample: data[sample]["fastq2"]
                           for sample in data.keys()}

    output_bam_filenames = {sample: data[sample]["output"]
                            for sample in data.keys()}

    return fastq1_filenames, fastq2_filenames, output_bam_filenames


def main():

    parser = argparse.ArgumentParser()

    pypeliner.app.add_arguments(parser)

    parser.add_argument('--config_file', required=True)

    parser.add_argument('--input_file', required=True)

    parser.add_argument('--out_dir', required=True)

    args = vars(parser.parse_args())

    with open(args['config_file']) as fh:
        config = yaml.load(fh)

    fastq1_inputs, fastq2_inputs, bam_outputs = read_bams(args['input_file'])

    workflow = alignment.align_samples(
        config,
        fastq1_inputs,
        fastq2_inputs,
        bam_outputs,
        args['out_dir']
    )

    pyp = pypeliner.app.Pypeline(config=args)

    pyp.run(workflow)


if __name__ == '__main__':
    main()
