import pypeliner


def markdups(input, output, metrics, tempdir):
    cmd = ['picard', '-Xmx4G', '-Xms4G',
            '-XX:ParallelGCThreads=1',
            'MarkDuplicates',
            'INPUT=' + input,
            'OUTPUT=' + output,
            'METRICS_FILE=' + metrics,
            'REMOVE_DUPLICATES=False',
            'ASSUME_SORTED=True',
            'VALIDATION_STRINGENCY=LENIENT',
            'TMP_DIR=' + tempdir,
            'MAX_RECORDS_IN_RAM=150000'
            ]

    pypeliner.commandline.execute(*cmd)
